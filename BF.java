import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class BF {
	private interface Visitor {
		void visit (Loop loop);
		void visit (Left left);
		void visit (Right right);
		void visit (Increment increment);
		void visit (Decrement decrement);
		void visit (Input input);
		void visit (Output output);
		void visit (Program program);
		
		void visit (Declaration declaration);
		void visit (Procedure procedure);
	}
	
	private interface Node {
		void accept (Visitor v);
	}

	/**
	 * Decrement pointer (move pointer left)
	 */
	private class Left implements Node {
		public void accept (Visitor v) {
			v.visit(this);
		}
	}
	
	/**
	 * Increment pointer (move pointer right)
	 */
	private class Right implements Node {
		public void accept (Visitor v) {
			v.visit(this);
		}
	}
	
	/**
	 * Increment byte at pointer
	 */
	private class Increment implements Node {
		public void accept (Visitor v) {
			v.visit(this);
		}
	}
	
	/**
	 * Decrement byte at pointer.
	 */
	private class Decrement implements Node {
		public void accept (Visitor v) {
			v.visit(this);
		}
	}
	
	//input and output are exactly the same...
	/**
	 * Input array[pointer]
	 */
	private class Input implements Node {
		public void accept (Visitor v) {
			v.visit(this);
		}
	}
	
	//nothing is outputted...?
	/**
	 * Output array[pointer]
	 */
	private class Output implements Node {
		public void accept (Visitor v) {
			v.visit(this);
		}
	}
	
	//Figure out what this does
	/**
	 * Short one line description.
	 */
	private class Sequence implements Node {
		private LinkedList<Node> children;
		public Sequence() {
			children = new LinkedList<Node>();
		}
		public void accept (Visitor v) {
			for (Node child : children) {
				child.accept(v);
			}
		}
		public void add (Node instruction) {
			children.add(instruction);
		}
	}


	/**
	 * [ = If array[pointer] is  zero, jump past matching ]
	 * ] = jump back to matching [
	 */
	private class Loop implements Node {
		public Node body;
		public Loop (Node body) {
			this.body = body;
		}
		public void accept (Visitor v) {
			v.visit (this);
		}
	}
	
    private class Procedure implements Node {

        public void accept(Visitor v) {
            v.visit(this);
        }
    }
	
	private class Declaration implements Node {
		public Node body;
		public Declaration(Node body) {
			this.body = body;
		}
		
		public void accept (Visitor v) {
			v.visit(this);
		}
	}

	
	//what is the difference between Program and Loop?
	/**
	 * Short one line description.
	 * 
	 * Longer description.
	 * 
	 * @param variable Description.
	 * @return Description.
	 */
	public static class Program implements Node {
		public Node body;
		public Program (Node body) {
			this.body = body;
		}
		public void accept(Visitor v) {
			v.visit (this);
		}
	}
	

	//I am really confused about the brainfuck syntax as various interpreters
	//have differing syntaxes
	//i.e. http://mazonka.googlecode.com/svn/trunk/mazgoo/bff4/readme.txt
	private int i = 0;
	private Sequence doParse (String str) {
		
		Sequence seq = new Sequence();
		char c;
		
		//really weird way to loop through a string...
		while (i < str.length ()) { 
			c = str.charAt (i);
			i++;
			
			if (Character.isDigit(c)) {
				//Looping Check
				String tmpDigit = "";
				char d = c;
				for(int j=i; Character.isDigit(d); j++) {
					tmpDigit+=d;
					d = str.charAt(j);
					//should I just increment i?
				}
				int repeatNum = Integer.parseInt(tmpDigit);
				for(int k=0; k<repeatNum; k++) {
					this.doParse(str); //This might be wrong...
					i++;
				}
			}

			if (c == '<') seq.add (new Left ());
			if (c == '>') seq.add (new Right ());
			if (c == '+') seq.add (new Increment ());
			if (c == '-') seq.add (new Decrement ());
			if (c == '.') seq.add (new Output ());
			if (c == ',') seq.add (new Input ());
			if (c == '[') seq.add (new Loop (doParse (str))); //Don't really follow this
			
			if (c == ']') return seq; //Change this to check for repetitions?
			if (c == ':') seq.add (new Procedure());
			if (c == '(') seq.add (new Declaration (doParse (str)));
			if (c == ')') return seq;
		}
		return seq;
	}
	
	/**
	 * Takes in BF String sequence
	 * Returns Program - a parsed BrainFuck program 
	 */
	public static Program parse (String str) {
		return new Program (new BF().doParse(str));
	}
	
	
	/*
	 * 30,000 byte array of elements initialized to zero
	 * I/O in ASCII encoding
	 * 
	 * Test Programs @ http://esoteric.sange.fi/brainfuck/bf-source/prog/
	 * - Maybe it would be worth looking for an interpreter
	 */
	public static void main(String[] args) {
		boolean BUG = false;
		/*
		 * 1. Increment pointer to 1
		 * 2. loop: array[pointer] until array[pointer] = 0 (does EOF return 0?)
		 */
		Node ascii = BF.parse("+[.+]");
		ascii.accept(new BF.InterpreterVisitor());
		if (BUG) {
			System.out.println("\nSanity Check... Checking tree structure");
			ascii.accept(new BF.PrintVisitor());
		}
		/*
		Node hello = BF.parse("++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.");
		hello.accept(new BF.InterpreterVisitor());
		Node beer = BF.parse(">+++++++++[<+++++++++++>-]<[>[-]>[-]<<[>+>+<<-]>>[<<+>>-]>>>[-]<<<+++++++++<[>>>+<<[>+>[-]<<-]>[<+>-]>[<<++++++++++>>>+<-]<<-<-]+++++++++>[<->-]>>+>[<[-]<<+>>>-]>[-]+<<[>+>-<<-]<<<[>>+>+<<<-]>>>[<<<+>>>-]>[<+>-]<<-[>[-]<[-]]>>+<[>[-]<-]<++++++++[<++++++<++++++>>-]>>>[>+>+<<-]>>[<<+>>-]<[<<<<<.>>>>>-]<<<<<<.>>[-]>[-]++++[<++++++++>-]<.>++++[<++++++++>-]<++.>+++++[<+++++++++>-]<.><+++++..--------.-------.>>[>>+>+<<<-]>>>[<<<+>>>-]<[<<<<++++++++++++++.>>>>-]<<<<[-]>++++[<++++++++>-]<.>+++++++++[<+++++++++>-]<--.---------.>+++++++[<---------->-]<.>++++++[<+++++++++++>-]<.+++..+++++++++++++.>++++++++[<---------->-]<--.>+++++++++[<+++++++++>-]<--.-.>++++++++[<---------->-]<++.>++++++++[<++++++++++>-]<++++.------------.---.>+++++++[<---------->-]<+.>++++++++[<+++++++++++>-]<-.>++[<----------->-]<.+++++++++++..>+++++++++[<---------->-]<-----.---.>>>[>+>+<<-]>>[<<+>>-]<[<<<<<.>>>>>-]<<<<<<.>>>++++[<++++++>-]<--.>++++[<++++++++>-]<++.>+++++[<+++++++++>-]<.><+++++..--------.-------.>>[>>+>+<<<-]>>>[<<<+>>>-]<[<<<<++++++++++++++.>>>>-]<<<<[-]>++++[<++++++++>-]<.>+++++++++[<+++++++++>-]<--.---------.>+++++++[<---------->-]<.>++++++[<+++++++++++>-]<.+++..+++++++++++++.>++++++++++[<---------->-]<-.---.>+++++++[<++++++++++>-]<++++.+++++++++++++.++++++++++.------.>+++++++[<---------->-]<+.>++++++++[<++++++++++>-]<-.-.---------.>+++++++[<---------->-]<+.>+++++++[<++++++++++>-]<--.+++++++++++.++++++++.---------.>++++++++[<---------->-]<++.>+++++[<+++++++++++++>-]<.+++++++++++++.----------.>+++++++[<---------->-]<++.>++++++++[<++++++++++>-]<.>+++[<----->-]<.>+++[<++++++>-]<..>+++++++++[<--------->-]<--.>+++++++[<++++++++++>-]<+++.+++++++++++.>++++++++[<----------->-]<++++.>+++++[<+++++++++++++>-]<.>+++[<++++++>-]<-.---.++++++.-------.----------.>++++++++[<----------->-]<+.---.[-]<<<->[-]>[-]<<[>+>+<<-]>>[<<+>>-]>>>[-]<<<+++++++++<[>>>+<<[>+>[-]<<-]>[<+>-]>[<<++++++++++>>>+<-]<<-<-]+++++++++>[<->-]>>+>[<[-]<<+>>>-]>[-]+<<[>+>-<<-]<<<[>>+>+<<<-]>>>[<<<+>>>-]<>>[<+>-]<<-[>[-]<[-]]>>+<[>[-]<-]<++++++++[<++++++<++++++>>-]>>>[>+>+<<-]>>[<<+>>-]<[<<<<<.>>>>>-]<<<<<<.>>[-]>[-]++++[<++++++++>-]<.>++++[<++++++++>-]<++.>+++++[<+++++++++>-]<.><+++++..--------.-------.>>[>>+>+<<<-]>>>[<<<+>>>-]<[<<<<++++++++++++++.>>>>-]<<<<[-]>++++[<++++++++>-]<.>+++++++++[<+++++++++>-]<--.---------.>+++++++[<---------->-]<.>++++++[<+++++++++++>-]<.+++..+++++++++++++.>++++++++[<---------->-]<--.>+++++++++[<+++++++++>-]<--.-.>++++++++[<---------->-]<++.>++++++++[<++++++++++>-]<++++.------------.---.>+++++++[<---------->-]<+.>++++++++[<+++++++++++>-]<-.>++[<----------->-]<.+++++++++++..>+++++++++[<---------->-]<-----.---.+++.---.[-]<<<]");
		beer.accept(new BF.InterpreterVisitor());
		Node fibonacci = BF.parse("+++++++++++>+>>>>++++++++++++++++++++++++++++++++++++++++++++>++++++++++++++++++++++++++++++++<<<<<<[>[>>>>>>+>+<<<<<<<-]>>>>>>>[<<<<<<<+>>>>>>>-]<[>++++++++++[-<-[>>+>+<<<-]>>>[<<<+>>>-]+<[>[-]<[-]]>[<<[>>>+<<<-]>>[-]]<<]>>>[>>+>+<<<-]>>>[<<<+>>>-]+<[>[-]<[-]]>[<<+>>[-]]<<<<<<<]>>>>>[++++++++++++++++++++++++++++++++++++++++++++++++.[-]]++++++++++<[->-<]>++++++++++++++++++++++++++++++++++++++++++++++++.[-]<<<<<<<<<<<<[>>>+>+<<<<-]>>>>[<<<<+>>>>-]<-[>>.>.<<<[-]]<<[>>+>+<<<-]>>>[<<<+>>>-]<<[<+>-]>[<+>-]<<<-]");
		fibonacci.accept(new BF.InterpreterVisitor());
		Node rot13 = BF.parse("+[,+[-[>+>+<<-]>[<+>-]+>>++++++++[<-------->-]<-[<[-]>>>+[<+<+>>-]<[>+<-]<[<++>>>+[<+<->>-]<[>+<-]]>[<]<]>>[-]<<<[[-]<[>>+>+<<<-]>>[<<+>>-]>>++++++++[<-------->-]<->>++++[<++++++++>-]<-<[>>>+<<[>+>[-]<<-]>[<+>-]>[<<<<<+>>>>++++[<++++++++>-]>-]<<-<-]>[<<<<[-]>>>>[<<<<->>>>-]]<<++++[<<++++++++>>-]<<-[>>+>+<<<-]>>[<<+>>-]+>>+++++[<----->-]<-[<[-]>>>+[<+<->>-]<[>+<-]<[<++>>>+[<+<+>>-]<[>+<-]]>[<]<]>>[-]<<<[[-]<<[>>+>+<<<-]>>[<<+>>-]+>------------[<[-]>>>+[<+<->>-]<[>+<-]<[<++>>>+[<+<+>>-]<[>+<-]]>[<]<]>>[-]<<<<<------------->>[[-]+++++[<<+++++>>-]<<+>>]<[>++++[<<++++++++>>-]<-]>]<[-]++++++++[<++++++++>-]<+>]<.[-]+>>+<]>[[-]<]<]");
		rot13.accept(new BF.InterpreterVisitor());
		*/
	}
	/* Lab Prob 5. If the PrintVisitor prints the original Brainfuck program given
	 * to BF.parse, then you correctly parsed the code.
	 * 
	 */
	public static class PrintVisitor implements Visitor {
		public void visit (Left n) { System.out.print('<'); }
		public void visit (Right n) { System.out.print('>'); }
		public void visit (Increment n) { System.out.print('+'); }
		public void visit (Decrement n) { System.out.print('-'); }
		public void visit (Input n) { System.out.print(','); }
		public void visit (Output n) { System.out.print('.'); }
		
		//It looks like a lot of implementations of brainfuck use '<' and '>'
		//to denote looping
		public void visit (Loop n) {
			System.out.print('[');
			n.body.accept(this);
			System.out.print(']');
		}
		public void visit (Program n) { n.body.accept(this); }

		//TODO invoke Declaration?
		public void visit(Declaration declaration) {
			// Not sure what I would output...
			System.out.print('(');
			declaration.body.accept(this);
		}

		//@Override -- why? What will this decorator do?
		public void visit(Procedure procedure) {
			//Why only return this?
			System.out.print(')');
		}
	}
	
	public static class InterpreterVisitor implements Visitor {
		private byte[] array;
		private int pointer;
		Procedure[] procedures;

		public void visit (Left n) { pointer--; }
		public void visit (Right n) { pointer++; }
		public void visit (Increment n) { array[pointer]++; }
		public void visit (Decrement n) { array[pointer]--; }
		public void visit (Input n) {
			try {
				array[pointer] = (byte) System.in.read();
			} catch (IOException e) {
				//Not Catching Anything?
			}
		}

		public void visit (Output n) {
			System.out.print((char)array[pointer]);
		}

		public void visit (Loop n) {
			while (array[pointer] != 0) {
				n.body.accept(this);
			}
		}
		
		public void visit (Declaration n) {
			final InterpreterVisitor that = this;
			final Declaration o = n;
			
			procedures[array[pointer]] = new Procedure() {
				public void execute() {
					o.body.accept(that);
				}
			};
		}

		public void visit (Program n) {
			array = new byte[30000];
			pointer = 0;
			procedures = new Procedure[256]; //annon names
			
			n.body.accept(this);
		}

		public interface Procedure {
		    void execute();
		}
		//Why create the interface within InterpreterVisitor?
		//This is why Eclipse yells at you when you try the following
		public void visit(BF.Procedure procedure) {
			procedures[array[pointer]].execute();
		}//instead of
		public void visit(Procedure procedure) {
			procedures[array[pointer]].execute();
		}
		
		
		

	}	
}
